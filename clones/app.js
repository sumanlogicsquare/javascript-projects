const express = require ('express');
// const session = require('express-session');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./model/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const JWT_SECRET = "5UmaN@CharYYa";

mongoose.connect('mongodb://localhost:27017/login-app-db', {
	useNewUrlParser: true,
	useUnifiedTopology: true
	// useCreateIndex: true
});

const app = express();
const port = process.env.PORT || 4051;
app.use(bodyParser.json());

// Set View Engine
app.set('views', './views');
app.set('view engine', 'ejs');

// Static View Generator
app.use(express.static('public'));
app.use('/css', express.static(__dirname + '/public/css'));
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/assets', express.static(__dirname + '/public/assets'));

app.get('/', (req, resp) => {
    // resp.sendFile(path.join(__dirname, '/index.html'));
    resp.render('index');
});


app.post('/login', async (req, resp) => {
    const {email, password} = req.body;
    const user = await User.findOne({ email }).lean();

    if(!user){
        return resp.json({ status: "error", error: "Username doesn't  exist...!" })
    }

    if(await bcrypt.compare(password, user.password)){
        const token = jwt.sign(
            { id: user._id, email: user.email }, 
            JWT_SECRET
        );

        return resp.json({ status: "OK", data: token });
    }

    resp.json({ status: "error", derror: "Username doesn't  exist...!" });

});


app.post('/register', async (req, resp) => {
    // console.log(req.body);
    // const salt = bcrypt.genSalt(10);

	const { username, email, password: plainTextPassword } = req.body;
    // console.log(bcrypt.hash(password, 'salt'));

    // const password = await bcrypt.hash(password, 10);
    // console.log(await bcrypt.hash(password, 10));
    const password = await bcrypt.hash(plainTextPassword, 10)

    // const response = await User.create({
    //         username,
    //         email,
    //         password
    //     });

    // console.log(response);

    if(!username || typeof username !== 'string'){
        return resp.json({ status: "error", error: "Error with Username" });
    }
    if(!email || typeof email !== 'string'){
        return resp.json({ status: "error", error: "Error with Email" });
    }
    if(!plainTextPassword || typeof plainTextPassword !== 'string'){
        return resp.json({ status: "error", error: "Error with Password" });
    }
    if(plainTextPassword.length <= 7){
        return resp.json({ status: "error", error: "Password length is small, Should be atleast 8 characters" });
    }

    try {
        const response = await User.create({
            username,
            email,
            password
        });
        console.log("Success", response);
    } catch (err) {
        console.log(err.message);
        console.log(JSON.stringify(err));
        if(err.code === 11000){
            return resp.json({ 
                status: "error",
                error: "Username or Email already exist!"
            });
        }
        throw err;
    }

    resp.json({status: "OK"});
});

app.get('/dashboard', (req, resp) => {
    // const { token } = req.body;
    // console.log(token);
    
    // const user = jwt.verify(token, JWT_SECRET);

    resp.render('dashboard', { name: "user.email" });
});

app.post('/change-pass', async(req, resp) => {
    // resp.sendFile(path.join(__dirname, '/index.html'));
    
    const { token, newPass } = req.body;
    // console.log({token});
    try {
        const user = jwt.verify(token, JWT_SECRET);
        // console.log("Decoded JWT :> ", user);

        const _id = user.id;
        const hashedPass = await bcrypt.hash(newPass)

        await User.updateOne({_id}, {
            $set: { password: hashedPass}
        });

    }catch (err){
        resp.json({status: "error", error: "Error found"});
    }
    // console.log(user.email);
    
    resp.json({ status: "OK" })
    // resp.render('dashboard', { name: user.email });


});


app.get('/songs', (req, resp) => {
    // resp.sendFile(path.join(__dirname, '/index.html'));
    resp.render('songs', { name: "Suman Acharyya" });
});


app.listen(port, () => {
    console.log('Server --->  http://localhost:' + port);   
});
