// regForm
// logForm

// Login Event Function
const loginForm = document.getElementById("logForm");
loginForm.addEventListener("submit", loginUser);

async function loginUser(event){
    event.preventDefault();
    console.log("Suman ******************");
    // const username = document.getElementById("userName").value;
    const email = document.getElementById("logEmail").value;
    const password = document.getElementById("logPassword").value;
    // console.log(username);
    const result = await fetch('/login', {
        method : 'POST',
        headers : {
            'Content-Type' : 'application/json',
        },
        body : JSON.stringify({
            email,
            password
        })
    }).then((resp) => resp.json());
    // console.log(result);
    if(result.status === "OK"){
        console.log("Token --> ", result.data);

        localStorage.setItem('token', result.data);
        alert("User LoggedIn Successfully!");
    }else{
        alert(result.error);
    }
}




// Register Event Function

const registrationForm = document.getElementById("regForm");
registrationForm.addEventListener("submit", registerUser);

async function registerUser(event){
    event.preventDefault();
    const username = document.getElementById("userName").value;
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    // console.log(username);
    const result = await fetch('/register', {
        method : 'POST',
        headers : {
            'Content-Type' : 'application/json',
        },
        body : JSON.stringify({
            username,
            email,
            password
        })
    }).then((resp) => resp.json());
    // console.log(result);
    if(result.status === "OK"){
        alert("User Created!");
    }else{
        alert(result.error);
    }
}





