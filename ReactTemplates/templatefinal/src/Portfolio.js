import React from 'react';

import img1f from "./assets/img/portfolio/fullsize/1.jpg";
import img1 from "./assets/img/portfolio/thumbnails/1.jpg";
import img2f from "./assets/img/portfolio/fullsize/2.jpg";
import img2 from "./assets/img/portfolio/thumbnails/2.jpg";
import img3f from "./assets/img/portfolio/fullsize/3.jpg";
import img3 from "./assets/img/portfolio/thumbnails/3.jpg";
import img4f from "./assets/img/portfolio/fullsize/4.jpg";
import img4 from "./assets/img/portfolio/thumbnails/4.jpg";
import img5f from "./assets/img/portfolio/fullsize/5.jpg";
import img5 from "./assets/img/portfolio/thumbnails/5.jpg";
import img6f from "./assets/img/portfolio/fullsize/6.jpg";
import img6 from "./assets/img/portfolio/thumbnails/6.jpg";

const Portfolio = () => {
    return(
        <div id="portfolio">
            <div className="container-fluid p-0">
                <div className="row g-0">
                    <div className="col-lg-4 col-sm-6">
                        <a className="portfolio-box" href={img1f} title="Project Name">
                            <img className="img-fluid" src={img1} alt="..." />
                            <div className="portfolio-box-caption">
                                <div className="project-category text-white-50">Category</div>
                                <div className="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <a className="portfolio-box" href={img2f} title="Project Name">
                            <img className="img-fluid" src={img2} alt="..." />
                            <div className="portfolio-box-caption">
                                <div className="project-category text-white-50">Category</div>
                                <div className="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <a className="portfolio-box" href={img3f} title="Project Name">
                            <img className="img-fluid" src={img3} alt="..." />
                            <div className="portfolio-box-caption">
                                <div className="project-category text-white-50">Category</div>
                                <div className="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <a className="portfolio-box" href={img4f} title="Project Name">
                            <img className="img-fluid" src={img4} alt="..." />
                            <div className="portfolio-box-caption">
                                <div className="project-category text-white-50">Category</div>
                                <div className="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <a className="portfolio-box" href={img5f} title="Project Name">
                            <img className="img-fluid" src={img5} alt="..." />
                            <div className="portfolio-box-caption">
                                <div className="project-category text-white-50">Category</div>
                                <div className="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a className="portfolio-box" href={img6f} title="Project Name">
                            <img className="img-fluid" src={img6} alt="..." />
                            <div className="portfolio-box-caption p-3">
                                <div className="project-category text-white-50">Category</div>
                                <div className="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>


    )
}


export default Portfolio;