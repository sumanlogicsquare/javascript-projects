import React from 'react';

import NavBar from './Navbar';
import Head from './Head';
import About from './About';
import Services from './Services';
import Portfolio from './Portfolio';
import Cot from './Calltoact';
import Contact from './Contact';
import Footer from './Footer';


import './style.css';

const App = () => {
    return(
        <div>
            <NavBar/>
            <Head/>
            <About/>
            <Services/>
            <Portfolio/>
            <Cot/>
            <Contact/>
            <Footer/>
        </div>
    )
}

export default App;