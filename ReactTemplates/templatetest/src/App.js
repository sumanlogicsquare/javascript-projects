import React from "react";

import NavBar from './Navbar';
import Jumbotron from './Jumbotron';
import Feature from './Feature';
import Cards from './Cards';
import Footer from './Footer';

const App = () => {
    return(
        <div style={{overflow:"hidden"}}>
            <NavBar />
            <Jumbotron />
            <Feature />
            <Cards />
            <Footer />
            {/* <h1>Suman Acharyya</h1> */}
        </div>
    )
}

export default App;