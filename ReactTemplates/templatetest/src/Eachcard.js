import React from 'react';

const EachCard = ({ title="Nothing!", img="https://images.pexels.com/photos/3532557/pexels-photo-3532557.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500", body="Nothing", button="Nothing!"}) => {
    return(
        <div className="card" style={{width: "18rem"}}>
            <img 
                src={img}
                className="card-img-top"
                alt="Card Image"
            />
            <div className="card-body">
                <h5 className="card-title">{title}</h5>
                <p className="card-text">
                {body}
                </p>
                <a href="#" className="btn btn-success">
                {button}
                </a>
            </div>
        </div>
    )
}

export default EachCard;