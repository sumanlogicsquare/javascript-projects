import React from "react";
import EachCard from "./Eachcard";

const Cards = () => {
  return (
    <div>
      <section className="contact bg-success ">
        <div className="container ">
          <h2 className="text-white">We love new friends!</h2>

          <div className="row">
            <div className="col-4">
              <EachCard
                title="Card Title"
                image="https://images.pexels.com/photos/3532557/pexels-photo-3532557.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                body="Some quick example text to build on the card title and make
                    up the bulk of the card's content. Some quick example text to build on the card title and make
                    up the bulk of the card's content."
                button="Button Name"
              />
            </div>

            <div className="col-4">
              <EachCard
                title="Card Title"
                image="https://images.pexels.com/photos/3532544/pexels-photo-3532544.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                body="Some quick example text to build on the card title and make
                    up the bulk of the card's content."
                button="Button Name"
              />
            </div>

            <div className="col-4">
              <EachCard
                title="Card Title"
                image="https://images.pexels.com/photos/2522663/pexels-photo-2522663.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                body="Some quick example text to build on the card title and make
                    up the bulk of the card's content."
                button="Button Name"
              />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Cards;
