import react from 'react';


const Button = ({title="Nothing!"}) => (
        <button className='btn'>{title}</button> 
)

export default Button;