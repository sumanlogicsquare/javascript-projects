import react from 'react';
import "./style.css";
import Button from './Button';


const App = () => {
    return (
        <div>
            <h1>Welcome</h1>
            <Button title="click here - 1"/>
            <Button title="click here - 2"/>
            <Button />
            {/* <button className='btn'>Click Here</button> 
            <button className='btn'>Click Here</button> */}
        </div>
    )
}


export default App;