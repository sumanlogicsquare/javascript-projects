const carouselSlider = document.querySelector(".carousel-slider");
const carouselImages = document.querySelectorAll(".carousel-slider img");
const prevBtn = document.getElementById("prevBtn");
const nextBtn = document.getElementById("nextBtn");

let ct = 1;
const size = carouselImages[0].clientWidth;

carouselSlider.style.transform = "translateX(" + (-size * ct) + "px)";

prevBtn.addEventListener("click", () => {
    if(ct <= 0) return;
    carouselSlider.style.transition = "transform 0.6s ease-in-out";
    ct--;
    carouselSlider.style.transform = "translateX(" + (-size * ct) + "px)";
})
nextBtn.addEventListener("click", () => {
    if(ct >= carouselImages.length - 1) return;
    carouselSlider.style.transition = "transform 0.6s ease-in-out";
    ct++;
    carouselSlider.style.transform = "translateX(" + (-size * ct) + "px)";
})

carouselSlider.addEventListener("transitionend", () => {
    if(carouselImages[ct].id === "firstClone"){
        carouselSlider.style.transition = "none";
        ct = carouselImages.length - ct;
        carouselSlider.style.transform = "translateX(" + (-size * ct) + "px)";
    };
    if(carouselImages[ct].id === "lastClone"){
        carouselSlider.style.transition = "none";
        ct = carouselImages.length - 2;
        carouselSlider.style.transform = "translateX(" + (-size * ct) + "px)";
    }

})