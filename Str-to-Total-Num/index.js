// let str = "onetwothreetwoonetentenninetenten"
// one: 2 && Two: 2&& Three: 1 && Ten: 4 && Nine: 1
// let str = "oetinnreennetentwntetwooonetheten" O'P:
// const str = "onetwothreetwoonetentenninetenten";


// Took The String
const str =
  "one One twO Two Two twothree    foUr fourfoUr fourfoUr four five sIx sixseven eiGht ninetEN ten ten   ninE";
// Converted to lowerCase
let tempUpdateStr = str.toLowerCase();

// Removed the Spaces
let updateStr = tempUpdateStr.replace(/\s/g, "");

// Made a Random Jumbled/Shuffled Word
shuffle = (str) =>
  [...str]
    .reduceRight(
      (res, _, __, arr) => (
        res.push(...arr.splice(0 | (Math.random() * arr.length), 1)), res
      ),
      []
    )
    .join("");

var shuffStr = shuffle(updateStr);

// TODO : Optimized Method
let num = [10];
let result = [];

let counter = (c) => {
  return shuffStr.match(new RegExp(c, "gi")).length;
};

num[1] = counter("w"); //2
num[3] = counter("u"); //4
num[5] = counter("x"); //6
num[7] = counter("g"); //8
num[0] = counter("o") - (num[1] + num[3]); //1
num[2] = counter("h") - num[7]; //3
num[4] = counter("f") - num[3]; //5
num[6] = counter("s") - num[5]; //7
num[8] = counter("i") - (num[4] + num[5] + num[7]); //9
num[9] = counter("n") - (num[0] + num[6] + num[8] * 2); //10

console.log("=================================");
console.log("Most Optimised Way");
console.warn(`One --> ${num[0]}
Two --> ${num[1]}
Three --> ${num[2]}
Four --> ${num[3]}
Five --> ${num[4]}
Six --> ${num[5]}
Seven --> ${num[6]}
Eight --> ${num[7]}
Nine --> ${num[8]}
Ten --> ${num[9]}`);
console.log("=================================");



// TODO : Another Method

console.log("Less Optimised Way");
let one = 0,
  two = 0,
  three = 0,
  four = 0,
  five = 0,
  six = 0,
  seven = 0,
  eight = 0,
  nine = 0,
  ten = 0;
  
function update(str, times, ...key) {
  // console.log(key);
  for (let i=0; i<times; i++){
    key.forEach(element => {
      str = str.replace(element, "");
    }); 
  }
  return str;
}

function checkTwo(shuffStr) {
  if (shuffStr.match("w").index >= 0) {
    let temp = (shuffStr.match(/w/g) || []).length;
    two = temp;
    // shuffStr = update(shuffStr, temp, "t", "w", "o");

    for(let i=0; i<temp; i++)
      shuffStr = shuffStr.replace('t','').replace('w','').replace('o','');

    return shuffStr;
  }
}
console.log(shuffStr);
shuffStr = checkTwo(shuffStr);
console.log(shuffStr);

// TODO : Another Sub Method
let newStr = shuffStr;
for(let data  of shuffStr){
  const tempKey = data;
  if (tempKey === "u"){
    newStr = newStr.replace("f", "");
    newStr = newStr.replace("o", "");
    newStr = newStr.replace("u", "");
    newStr = newStr.replace("r", "");
    four += 1;
  }
}

shuffStr = newStr;
console.log(shuffStr);


function checkSix(shuffStr) {
  if (shuffStr.match("x").index >= 0) {
    let temp = (shuffStr.match(/x/g) || []).length;
    six = temp;
    // shuffStr = update(shuffStr, temp, "s", "i", "x");

    for(let i=0; i<temp; i++)
      shuffStr = shuffStr.replace('s','').replace('i','').replace('x','');

    return shuffStr;
  }
}
shuffStr = checkSix(shuffStr);
console.log(shuffStr);

function checkEight(shuffStr) {
  if (shuffStr.match("g").index >= 0) {
    let temp = (shuffStr.match(/g/g) || []).length;
    eight = temp;
    // shuffStr = update(shuffStr, temp, "e", "i", "g", "h", "t");

    for(let i=0; i<temp; i++)
      shuffStr = shuffStr.replace('e','').replace('i','').replace('g','').replace('h','').replace('t','');

    return shuffStr;
  }
}
shuffStr = checkEight(shuffStr);
console.log(shuffStr);


// TODO : Yet Another Sub Method

function checkOne(shuffStr) {
  if (shuffStr.match("o").index >= 0) {
    let temp = (shuffStr.match(/o/g) || []).length;
    one = temp;
    shuffStr = update(shuffStr, temp, "o", "n", "e");
    return shuffStr;
  }
}
shuffStr = checkOne(shuffStr);
console.log(shuffStr);

function checkThree(shuffStr) {
  if (shuffStr.match("h").index >= 0) {
    let temp = (shuffStr.match(/h/g) || []).length;
    three = temp;
    shuffStr = update(shuffStr, temp, "t", "h", "r", "e", "e");
    return shuffStr;
  }
}
shuffStr = checkThree(shuffStr);
console.log(shuffStr);

function checkFive(shuffStr) {
  if (shuffStr.match("f").index >= 0) {
    let temp = (shuffStr.match(/f/g) || []).length;
    five = temp;
    shuffStr = update(shuffStr, temp, "f", "i", "v", "e");
    return shuffStr;
  }
}
shuffStr = checkFive(shuffStr);
console.log(shuffStr);

function checkSeven(shuffStr) {
  if (shuffStr.match("s").index >= 0) {
    let temp = (shuffStr.match(/s/g) || []).length;
    seven = temp;
    shuffStr = update(shuffStr, temp, "s", "e", "v", "e", "n");
    return shuffStr;
  }
}
shuffStr = checkSeven(shuffStr);
console.log(shuffStr);

function checkNine(shuffStr) {
  if (shuffStr.match("i").index >= 0) {
    let temp = (shuffStr.match(/i/g) || []).length;
    nine = temp;
    shuffStr = update(shuffStr, temp, "n", "i", "n", "e");
    return shuffStr;
  }
}
shuffStr = checkNine(shuffStr);
console.log(shuffStr);

function checkTen(shuffStr) {
  if (shuffStr.match("n").index >= 0) {
    let temp = (shuffStr.match(/n/g) || []).length;
    ten = temp;
    shuffStr = update(shuffStr, temp, "t", "e", "n");
    return shuffStr;
  }
}
shuffStr = checkTen(shuffStr);
console.log(shuffStr);

console.log("=================================");
console.log("Most Optimised Way");
console.warn(`One --> ${one}
Two --> ${two}
Three --> ${three}
Four --> ${four}
Five --> ${five}
Six --> ${six}
Seven --> ${seven}
Eight --> ${eight}
Nine --> ${nine}
Ten --> ${ten}`);
console.log("=================================");


