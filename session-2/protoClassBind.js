const obj = {
    name : "Suman",
    address : "Barasat",
    getDetails : function (){
        console.log(`${this.name} from ${this.address}`);
    }
}

const obj2 = {
    name : "Acharyya",
}

obj2.__proto__ = obj;
console.log(obj2);
obj2.getDetails();


// display method for all functions 

Function.__proto__.display = function(data){
    this.data = data;
    return `Helllo ${this.data} from Function!`;
}

function display2 (){};
console.log(display2.display("world"));

