// "use strict"
//   import addFunction from "./module";

// console.log(addFunction(12, 34));

// function Animal() {

//  }

// Animal.prototype.speak = function() {
//   return this;
// }

// Animal.eat = function() {
//   return this;
// }

// let obj = new Animal();
// let speak = obj.speak;
// console.log(speak());

// let eat = Animal.eat;
// console.log(eat());

// class Student {
//   constructor(name, address){
//     this.name = name;
//     this.address = address;
//   }
//   getDetails(age){
//     return `${this.name} is with address ${this.address} with age ${age}`
//   }
// } 

// const suman = new Student("Suman Acharyya", "Kolkata")
// const someone = new Student("Someone Name", "Somewhare")

// console.log(suman.getDetails(24));
// console.log(suman.getDetails(200));



const fresher = {
  experience(){
    return 0;
  }
}

const degree = {
  hasDegree(){
    return true;
  }
}

const fullStack = {
  isFullStack(){
    return true;
  }
}

const developer = Object.assign({}, fresher, degree, fullStack);

console.log(developer.hasDegree());
console.log(developer.experience());
console.log(developer.isFullStack());
