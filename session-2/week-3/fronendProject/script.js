let button = document.getElementById("btn"); //
let input = document.getElementById("inputBox"); // add
let lists = document.getElementById("lists");
let element = document.getElementsByTagName("li");

button.addEventListener('click', () => {
    let ipVal = input.value;
    if(ipVal === ""){
        alert("Enter Some Value !");
    }
    else{
        let li = document.createElement("li");
        li.innerHTML = ipVal;
        lists.insertBefore(li, lists.childNodes[0]);
        input.value = "";
    }
});


lists.addEventListener("click", el => {
    if(el.target.tagName == 'LI'){
        el.target.classList.toggle('checked');
    }
})