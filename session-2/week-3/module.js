"use strict"

function addFunc(num1, num2){
    return num1+num2;
}

function mulFunc(num1, num2){
    return num1*num2;
}


export {
    addFunc as addFunction,
    mulFunc as mulFunction
  };
  
  

// const message = () => {
//     const name = "Jesse";
//     const age = 40;
//     return name + ' is ' + age + 'years old.';
//     };
    
// export default message;