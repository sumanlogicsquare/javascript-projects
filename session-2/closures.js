// TODO :  Closure

const ress =  function x (n1){
    return function (n2){
        return function (n3){
            let sum = (n1+n2+n3);
            return function (){
                console.log(sum);
            }
        }
    }
    return x;
}(10)(20)(30);
ress();




// TODO :  The "new Function"

// Using Constructor
function PrintVal(data){
    this.data = data;
    this.getPrintVal = function(){
        return `Returning Hello ${this.data}`;
    }
}

const valueGet = new PrintVal("World");
console.log(valueGet.getPrintVal());


// factorial functioon
function retVal (data){
    return {
        xyz (){
            return `Value is ${data}`;
        }
    }
}
const xyzz = retVal(100)
console.log(xyzz.xyz());




// TODO :  Rest Parameters & Spread Operator

function sum(x, y, ...args){ //Rest Operator
    let res = 0;
    let resMult = x * y;
    for(let data of args)
        res += data;
    return {
        Sum : res,
        Multiplication : resMult,
    };
}

const arrr = [10, 20, 30, 40, 50, 60, 90];
console.log(sum(...arrr)); // Spread Operator

// TODO :  Global Object

// window.alert("Hello");
// let helloValToShow = "World...!!"
// function  helloPrint(){
//     alert(`Hello ${helloValToShow}`);
// }
// window.helloPrint();



// TODO :  Function Object

const objFunc = function abcxyz (data){
    return `Hello ${data} from objFunc`;
}
objFunc.name;
objFunc.length;
objFunc.toString;


// TODO :  SetTimeOut & SetInterval
// TimeOut
function timeOutFunc(data){
    console.log("log from timeout : " + data);
};
const timeOutRes = setTimeout(timeOutFunc, 3000, "3000ms");
if(!true){
    clearTimeout(timeOutRes);
}


// Interval
let ct = 0;
function intervalFunc(data){
    console.log(`log with interval of ${data} count value :  ${ct++}`);
    
    if(ct>5)
        clearInterval(intervalFuncRes);
};
const intervalFuncRes = setInterval(intervalFunc, 1000, "1000ms", ct);


// TODO :  Recursion

let countRecur = 20;
let totalRecTime = 0;
function countDown(count){
    if(count == 1)
        return {
            status : `You have reached ${count}`,
            timeOfRec : ++totalRecTime,
        }
    else if(count <= 0)
        return {
            status : `time end`,
            timeOfRec : ++totalRecTime,
        }
    totalRecTime++;
    return countDown(count-1);
}

console.log(countDown(countRecur));





// EXTRAAAS

// TODO : Promise Async Await 

let me = "Suman";
const wake = name => {
    return `${name} waked up!`;
};

const breakFast = name => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve (`${name} doing breakfast!`);
            reject(new Error(`${name} not done the breakfast!`));
        }, 2000)
    })
};

const ready = name => {
    return `${name} ready for work!`;
};

const schedule = async nam => {
    console.log(wake(nam));
    
    // console.log(await breakFast(nam));
    await breakFast(nam).then(data => {
        console.log(data);
    }).catch(e => console.log(e));

    console.log(ready(nam));
};
schedule(me);

// ANOTHER WAY 

// let me = "Suman";
// const wake = name => {
//     return `${name} waked up!`;
// };

// const breakFast = (name, cb) => {
//     setTimeout(() => {
//         cb (`${name} doing breakfast!`);
//     }, 2000)
// }


// const ready = name => {
//     return `${name} ready for work!`;
// };

// const schedule = nam => {
//     console.log(wake(nam));
    
//     breakFast(nam, cbRet => {
//         console.log(cbRet) 
//     });

//     console.log(ready(nam));
    
// }
// schedule(me)



