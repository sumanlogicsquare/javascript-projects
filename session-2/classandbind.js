// TODO :  Classes

class UserScore {
    constructor(fName, lName, score){
        this.fName = fName;
        this.lName = lName;
        this.score = score;
    }
    getUserDetails(){
        return `${this.fName} ${this.lName} with score ${this.score}.`;
    }
    getStatus(){
        if(this.score > 59) return "Pass";
        else return "Fail";
    }
    editUserDetails(fullName, score){
        const name = fullName.split(" ");
        this.fName = name[0];
        this.lName = name[1];
        this.score = score;
    }
}

const suman1 = new UserScore("Suman", "Acharyya", 95);
console.log(suman1.getUserDetails());
console.log(suman1.getStatus());

suman1.editUserDetails("Suman Acharyya", 55);
console.log(suman1.getUserDetails());
console.log(suman1.getStatus());




// TODO : Bind

const userDetails = function(state, city){
    this.state = state;
    this.city = city;
    console.log(`${this.fName} ${this.lName} from state ${this.state} with city ${this.city}`);
}

const suman = {
    fName : "Suman",
    lName : "Acharyya",
}

const some = {
    fName : "Some",
    lName : "Another",
}
// using Bind
userDetails.bind(suman, "West Bengal", "Barasat")();

// using Call
userDetails.call(some, "Some State", "Some City");

// using Apply
userDetails.apply(some, ["Some State", "Some City"]);






// TODO : Prototypes

function User(fname, score){
    const name = fname.split(" ");
    this.fName = name[0];
    this.lName = name[1];
    this.score = score;
    this.getFullName = function(){
        return `Full name is ${this.fName} ${this.lName}`;
    }
}
User.prototype.getGrade = function (){
    if(this.score > 59) return "P&P";
    else return "Fail";
}
User.prototype.getScore = function (){
    console.log("Your Grade is : " + this.getGrade()); // Calling GetGrade 
    return `Score is ${this.score}`;

}

const sumannn  = new User("Suman Acharyya", 80);

if(sumannn.hasOwnProperty("getFullName")){
    console.log(sumannn.hasOwnProperty("getFullName"));
    
    console.log(sumannn.getFullName());
    console.log(sumannn.getScore());
}



// TODO : Proto Chaining

const Fruit = function (){};

Fruit.prototype = {
    display(){
        return "It's a fruit";
    }
};


const Color = function(){};

Color.prototype = Object.create(Fruit.prototype);

Color.prototype.display = function(col){
    this.col = col;
    return `This is ${this.col} colour fruit`;
};


const Season = function(){};

Season.prototype = Object.create(Color.prototype);

Season.prototype.display = function (col, season, frName){
    this.col = col;
    this.season = season;
    this.frName = frName;
    return `${this.frName} is a ${this.season} Seasonal ${this.col} colour fruit`;
};

const apple = new Fruit();
const red = new Color();
const summer = new Season();


console.log(apple.display());
console.log(red.display("Red"));
console.log(summer.display("Red", "Summer", "Apple"));