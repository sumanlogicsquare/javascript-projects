// Array.prototype.find()


const days = [
    {name: 'abc', days: 2},
    {name: 'def', days: 2},
    {name: 'ghi', days: 9},
    {name: 'jkl', days: 6},
    {name: 'mno', days: 8}
  ];
  
  const res = days.find( ({ name }) => name === "jkl");
  
  console.log(res);




// Array.prototype.filter()

const fruits = ['apple', 'banana', 'grapes', 'mango', 'orange'];

const filterItems = (arr, key) => {
  return arr.filter(el => el.toLowerCase().indexOf(key.toLowerCase()) > -1);
}
console.log(filterItems(fruits, "gr"));


// Callback with filter
let isOdd = (e) => e % 2 !== 0;

console.log([15, 13, 13, 9, 19].every(isOdd));

// Extras

const f1 = str => console.log(`Hello ${str}`);
const f2 = (arg, str) => arg(str);
f2(f1, "world");




// Array.prototype.map()

const keyvalArray = [
    { key: 1, value: 10 },
    { key: 2, value: 20 },
    { key: 3, value: 30 }
];
console.log(keyvalArray.map(({ key, value}) => ({ [key]: (value)*10 }))); 

const map = Array.prototype.map;
console.log( map.call('Hello World', (x) => x.charCodeAt(0))); // returns ASCII

const nameArray = [
    { key: "Su Ac", value: 10 },
    { key: "abc def", value: 20 },
    { key: "wxy zec", value: 30 }
];
console.log(nameArray.map(el => el.key.split(" ")[0]));




// Array.prototype.reduce()

const array = [15, 16, 17, 18, 19];

console.log(array.reduce((accumu, current) => accumu + current));

function reducer(previous, current, index, array) {
  const res = previous + current;
  console.log(`previous: ${previous}, current: ${current}, index: ${index}, returns: ${res}`);
  return res;
}

array.reduce(reducer);

const kvArr = [
    { key: 1, value: 10 },
    { key: 2, value: 20 },
    { key: 3, value: 30 }
];

console.log(kvArr.reduce(((accumu, current) => accumu += current.value), 0));


const kvArr1 = [
    { key: 1, value: 10 },
    { key: 4, value: 10 },
    { key: 2, value: 20 },
    { key: 3, value: 30 },
    { key: 5, value: 30 }
];

const getValue = kvArr1.reduce((accumu, current)=> {
    const currVal = current.value;
    console.log(currVal);
    if(accumu[currVal] == null) 
      accumu[currVal] = [];
    accumu[currVal].push(current);
    return accumu;
}, {});

console.log(getValue);


