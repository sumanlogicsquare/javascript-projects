import fetch from "node-fetch";


// TODO : Promise and error handling

// new Promise((resolve, reject) => {
//     // network -> Suceess / Fail
//     const succesreponse = "A success"
//     const failreponse = "A Fail"
//     if (false) {
//         resolve(succesreponse)
//     } else {
//         reject(failreponse)
//     }
// }).then(data => {
//     console.log("in then block", data)
//     throw new Error("error in 1st then block")
//     return new Promise((resolve, reject) => resolve("2nd Success"))
// }).then(data1 => {
//     console.log("in then block", data1)
// }).catch(error => {
//     console.log("in catch block", error)
// })

// then - catch
// async await 




// const myAsyincFunc = async () => {
//   const promiseToHandle = new Promise((resolve, reject) => {
//       // network -> Suceess / Fail
//       const succesreponse = "A success"
//       const failreponse = "A Fail -to customer"
//       if (false) {
//           resolve(succesreponse)
//       } else {
//           reject(failreponse)
//       }
//   })
//   try {
//       const promiseResponse = await promiseToHandle
//       console.log(promiseResponse)
//   } catch (error) {
//       console.log("catch block *****", error)
//   }
// }

// myAsyincFunc()





// 51.50853 -0.12574
// fetch("https://api.openweathermap.org/data/2.5/weather?lat=51.50853&lon=-0.12574&appid=082e42f7d4b6ef3b13116662578bb9ac");

// const place = "Delhi"
// fetch("https://api.openweathermap.org/data/2.5/weather?lat=28.38&lon=77.12&appid=082e42f7d4b6ef3b13116662578bb9ac");
// const key = "082e42f7d4b6ef3b13116662578bb9ac";
// const lat = 28.38;
// const lon = 77.12;
// const url = "https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${key}"


// using promise
fetch('https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/India,Kolkata?=CONVERT(A1,"C")&key=7VTDDNX254U66J6EUCTBKYSVD').then(resp => {
  if(resp.ok)
  // console.log(resp);
  return resp.json()
  else
    throw new error;
}).then(resp => {
  console.log(resp.days)}).catch(err => {
  console.log("Error >>> " + err)});


// Using async await
const myAsyincFunc = async () => {
  
  try {
    const weather = await fetch('https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/India,Kolkata?=CONVERT(A1,"C")&key=7VTDDNX254U66J6EUCTBKYSVD');
    const weatherPromise = weather.json();
    const response = await weatherPromise
    console.log(response.days)
  } catch (error) {
      console.log("Catch error is >>> ", error)
  }
}

myAsyincFunc()

navigate.geo.location