// Player Name Change

let p1 = "PLAYER1";
let p2 = "PLAYER2";

function playerName(){
    p1 = prompt("'O' Player Name:-");
    p2 = prompt("'X' Player Name:-");

    let  str1 = p1 + ": O";
    let  str2 = p2 + ": X";
    document.getElementById("result").innerHTML = str1 + "<br>" + str2;
    document.getElementById("result").style.color="blue";
}


var flag = true;
var c = 0;
function game(){
    if(flag == true){
        document.activeElement.innerHTML = "O";
        document.activeElement.removeAttribute("onclick");
        document.activeElement.style.backgroundColor = "teal";
        c++;
        flag = false;
        let final = checkResult();
        if(final == "stop"){
            document.getElementById("result").innerHTML = p1+" won the match :-)";
            document.getElementById("result").style.fontSize = "45px";
            document.getElementById("result").style.color = "seablue";
            document.getElementById("result").style.paddingTop = "1em";
            removeATTR();
        }
        else if(c == 9){
            document.getElementById("result").innerHTML = "Match Draw !";
        }
    }
    else{
        document.activeElement.innerHTML = "X";
        document.activeElement.removeAttribute("onclick");
        document.activeElement.style.backgroundColor = "seaGreen";
        c++;
        flag = true;
        let final = checkResult();
        if(final == "stop"){
            document.getElementById("result").innerHTML = p2+" won the match :-)";
            document.getElementById("result").style.fontSize = "45px";
            document.getElementById("result").style.color = "teal";
            document.getElementById("result").style.paddingTop = "1em";
            removeATTR();
        }
        else if(c == 9){
            document.getElementById("result").innerHTML = "Match Draw !";
            document.getElementById("result").style.paddingTop = "1em";
        }

    }
}


// Checking the Values With Each Boxes
function checkResult(){
    let str;
    if(
        (document.getElementById("box1").innerHTML == document.getElementById("box2").innerHTML && document.getElementById("box1").innerHTML == document.getElementById("box3").innerHTML) 
        && 
        ((document.getElementById("box1").innerHTML == "O") || (document.getElementById("box1").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box1").innerHTML == document.getElementById("box4").innerHTML && document.getElementById("box1").innerHTML == document.getElementById("box7").innerHTML) 
        && 
        ((document.getElementById("box1").innerHTML == "O") || (document.getElementById("box1").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box1").innerHTML == document.getElementById("box5").innerHTML && document.getElementById("box1").innerHTML == document.getElementById("box9").innerHTML) 
        && 
        ((document.getElementById("box1").innerHTML == "O") || (document.getElementById("box1").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box2").innerHTML == document.getElementById("box5").innerHTML && document.getElementById("box2").innerHTML == document.getElementById("box8").innerHTML) 
        && 
        ((document.getElementById("box2").innerHTML == "O") || (document.getElementById("box2").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box3").innerHTML == document.getElementById("box6").innerHTML && document.getElementById("box3").innerHTML == document.getElementById("box9").innerHTML) 
        && 
        ((document.getElementById("box3").innerHTML == "O") || (document.getElementById("box3").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box3").innerHTML == document.getElementById("box5").innerHTML && document.getElementById("box3").innerHTML == document.getElementById("box7").innerHTML) 
        && 
        ((document.getElementById("box3").innerHTML == "O") || (document.getElementById("box3").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box4").innerHTML == document.getElementById("box5").innerHTML && document.getElementById("box4").innerHTML == document.getElementById("box6").innerHTML) 
        && 
        ((document.getElementById("box4").innerHTML == "O") || (document.getElementById("box4").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }

    else if(
        (document.getElementById("box7").innerHTML == document.getElementById("box8").innerHTML && document.getElementById("box7").innerHTML == document.getElementById("box9").innerHTML) 
        && 
        ((document.getElementById("box7").innerHTML == "O") || (document.getElementById("box7").innerHTML == "X"))
    ){
        str = "stop";
        return str;
    }


}

// Remove Attribute

function removeATTR(){
    document.getElementById("box1").removeAttribute("onclick");
    document.getElementById("box2").removeAttribute("onclick");
    document.getElementById("box3").removeAttribute("onclick");
    document.getElementById("box4").removeAttribute("onclick");
    document.getElementById("box5").removeAttribute("onclick");
    document.getElementById("box6").removeAttribute("onclick");
    document.getElementById("box7").removeAttribute("onclick");
    document.getElementById("box8").removeAttribute("onclick");
    document.getElementById("box9").removeAttribute("onclick");

}

// Reset Game

function reset(){
    window.location.reload();
}
