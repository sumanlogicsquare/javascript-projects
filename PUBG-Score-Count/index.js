"use strict"
let user = {
    name : "",
    email : "",
    lastLogin : null,
    point : 0,
    dayFlag : false,
    dayCount : 0,
    skin : [],
}
const gifts = [100, 200, 300, 400, "Red"];

function check(){
    if (user.lastLogin !== null) 
        document.getElementById("preResDate").innerHTML = "Last Login Date >>> " + user.lastLogin.getFullYear() + " / " + (user.lastLogin.getMonth()+1) + " / " + user.lastLogin.getDate();
        

    let tempDate = new Date(document.getElementById("loginTime").value);
    
    const compareDate = Math.abs(user.lastLogin - tempDate);
    const differenceDays = Math.ceil(compareDate / (60 * 60 * 24 * 1000)); 

    if(user.lastLogin !== null){
        if( differenceDays > 1 ){
            if(user.dayCount === 4 && user.skin.length <= 0){
                user.lastLogin = tempDate;
                user.skin.push(gifts[4]);
                // user.point = 0;
                document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;
                document.getElementById("resSkin").innerHTML = "Player Skin >>> " + user.skin[user.skin.length - 1];
                user.dayCount = 0;
                user.lastLogin = null;
            }
            else{
                user.point += gifts[0];
                user.lastLogin = tempDate;
                user.dayCount = 1;
                
                document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;

            }
        }

        if( differenceDays === 1 ){
            if(user.dayCount === 1){
                // console.log(user.point);
                user.point += gifts[1];
                // console.log(user.point);
                user.lastLogin = tempDate;
                user.dayCount += 1;
                document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;
            }

            else if(user.dayCount === 2){
                user.point += gifts[2];
                user.dayCount += 1;
                user.lastLogin = tempDate;
                document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;
            }

            else if(user.dayCount === 3){
                user.point += gifts[3];
                user.dayCount += 1;
                user.lastLogin = tempDate;
                document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;
        
                document.getElementById("preResDate").innerHTML = "Last Login Date >>> " + user.lastLogin.getFullYear() + " / " + (user.lastLogin.getMonth()+1) + " / " + user.lastLogin.getDate();
            }

            else if(user.dayCount === 4 && user.skin.length <= 0){
                user.lastLogin = tempDate;
                // user.point = 0;
                user.skin.push(gifts[4]);
                document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;
                document.getElementById("resSkin").innerHTML = "Player Skin >>> " + user.skin[user.skin.length - 1];
                user.dayCount = 0;
                user.lastLogin = null;
            }
        }
    }
    
    else {
        user.point += gifts[0];
        user.lastLogin = tempDate;
        user.dayCount += 1;
        user.skin.pop();
        
        document.getElementById("resPoints").innerHTML = "Player Point >>> " + user.point;

    }
    user.name = document.getElementById("userName").value
    document.getElementById("resName").innerHTML = "Player Name >>> " + user.name;

    user.email = document.getElementById("userEmail").value
    document.getElementById("resEmail").innerHTML = "Player Email >>> " + user.email;
}
